package com.devcamp.provinceapi.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provinceapi.models.CWard;

public interface WardRepository extends JpaRepository<CWard, Integer> {

}
