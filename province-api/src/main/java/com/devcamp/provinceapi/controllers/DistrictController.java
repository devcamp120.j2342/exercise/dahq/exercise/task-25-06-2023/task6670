package com.devcamp.provinceapi.controllers;

import java.util.Optional;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provinceapi.models.CDistrict;
import com.devcamp.provinceapi.models.CProvince;
import com.devcamp.provinceapi.repositorys.DistrictRepository;
import com.devcamp.provinceapi.repositorys.ProvinceRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DistrictController {
    @Autowired
    DistrictRepository districtReponsitory;
    @Autowired
    ProvinceRepository provinceRepository;

    @PostMapping("/district/create/{id}")
    public ResponseEntity<Object> createDistrict(@PathVariable("id") int id, @RequestBody CDistrict pDistrict) {

        try {
            Optional<CProvince> cProvince = provinceRepository.findById(id);
            CDistrict district2 = new CDistrict();
            district2.setName(pDistrict.getName());
            district2.setPrefix(pDistrict.getPrefix());
            district2.setProvince(pDistrict.getProvince());
            district2.setWards(pDistrict.getWards());
            CProvince provinceById = cProvince.get();
            district2.setProvince(provinceById);
            CDistrict districtSave = districtReponsitory.save(district2);
            return new ResponseEntity<>(districtSave, HttpStatus.CREATED);

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }

    }

    @PutMapping("/district/update/{id}")
    public ResponseEntity<CDistrict> updateDistrict(@PathVariable("id") int id, @RequestBody CDistrict pDistrict) {
        Optional<CDistrict> cDistrict = districtReponsitory.findById(id);
        if (cDistrict.isPresent()) {
            try {
                cDistrict.get().setName(pDistrict.getName());
                cDistrict.get().setPrefix(pDistrict.getPrefix());
                cDistrict.get().setWards(pDistrict.getWards());
                return new ResponseEntity<>(districtReponsitory.save(cDistrict.get()), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<CDistrict>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/district/delete/{id}")
    public ResponseEntity<CDistrict> deleteDistrict(@PathVariable("id") int id) {
        districtReponsitory.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/district/all")
    public List<CDistrict> getAllDistrict() {
        return districtReponsitory.findAll();
    }

    @GetMapping("/district/details/{id}")
    public CDistrict getDistrictByID(@PathVariable("id") int id) {
        return districtReponsitory.findById(id).get();
    }

}
