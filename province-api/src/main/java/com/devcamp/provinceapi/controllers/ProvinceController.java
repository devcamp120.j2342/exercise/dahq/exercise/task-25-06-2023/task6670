package com.devcamp.provinceapi.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provinceapi.models.CProvince;
import com.devcamp.provinceapi.repositorys.ProvinceRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ProvinceController {
    @Autowired
    ProvinceRepository provinceRepository;

    @GetMapping("/province/all")
    public List<CProvince> getAllProvince() {
        return provinceRepository.findAll();
    }

    @GetMapping("/province/{name}")
    public CProvince getProvince(@PathVariable("name") String name) {
        CProvince province = provinceRepository.findByName(name);
        return province;
    }

    @GetMapping("/province/details/{id}")
    public CProvince getProvinceById(@PathVariable("id") int id) {
        return provinceRepository.findById(id).get();
    }

    @PostMapping("/province/create")
    public ResponseEntity<CProvince> createProvince(@RequestBody CProvince pCProvince) {
        try {
            return new ResponseEntity<>(provinceRepository.save(pCProvince), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/province/update/{id}")
    public ResponseEntity<CProvince> updateProvince(@RequestBody CProvince Pprovince, @PathVariable("id") int id) {
        Optional<CProvince> cCprovince = provinceRepository.findById(id);
        if (cCprovince.isPresent()) {
            try {
                cCprovince.get().setName(Pprovince.getName());
                cCprovince.get().setCode(Pprovince.getCode());
                cCprovince.get().setDistricts(Pprovince.getDistricts());
                return new ResponseEntity<>(provinceRepository.save(cCprovince.get()), HttpStatus.OK);

            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/province/delete/{id}")
    public ResponseEntity<CProvince> deleteProvince(@PathVariable("id") int id) {
        provinceRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
