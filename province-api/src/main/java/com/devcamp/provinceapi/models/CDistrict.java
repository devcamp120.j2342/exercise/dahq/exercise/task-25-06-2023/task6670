package com.devcamp.provinceapi.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "district")
public class CDistrict {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String prefix;
    @ManyToOne
    @JsonIgnore
    private CProvince province;
    @OneToMany(targetEntity = CWard.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "district_id")
    private Set<CWard> wards;

    public CDistrict() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Set<CWard> getWards() {
        return wards;
    }

    public void setWards(Set<CWard> wards) {
        this.wards = wards;
    }

    @JsonIgnore

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince cProvince) {
        this.province = cProvince;
    }

}
